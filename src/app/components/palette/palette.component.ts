import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.scss']
})
export class PaletteComponent {

  public colors: String[] = [
    "red", 
    "blue",
    "green"
  ];

  @Output() 
  selectColor = new EventEmitter<String>();

  onColorClick(color: String):void{
    this.selectColor.emit(color);
  }
  
}
