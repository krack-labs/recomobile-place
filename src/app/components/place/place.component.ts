import { Component } from '@angular/core';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent {
  public colors: String[] = [
    "red", 
    "blue",
    "green"
  ];
  public place: String[][] = [
    [
      "red", "blue", "green"
    ],
    [
      "green", "green", "green"
    ],
    [
      "blue", "blue", "green"
    ]
  ];

  public selectedI: number|null = null;
  public selectedJ: number|null = null;


  selectPixel(i: number, j: number): void{
    this.selectedI = i;
    this.selectedJ = j;
  }

  selectColor(color: String):void {
    if (this.selectedI !=null && this.selectedJ != null) {
      this.place[this.selectedI][this.selectedJ] = color;
      this.selectedI = null;
      this.selectedJ = null;
    }
  }
}
